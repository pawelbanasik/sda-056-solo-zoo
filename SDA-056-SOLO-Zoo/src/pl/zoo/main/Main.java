package pl.zoo.main;

import java.sql.Date;

import pl.zoo.animals.AnimalType;
import pl.zoo.animals.Turtle;
import pl.zoo.init.Zoo;
import pl.zoo.people.Staff;
import pl.zoo.people.Visitor;
import pl.zoo.tickets.Ticket;
import pl.zoo.tickets.TicketType;

public class Main {

	public static void main(String[] args) {

		Zoo zoo = new Zoo(100);

		Turtle turtle1 = new Turtle(AnimalType.REPTILE, 0);
		Turtle turtle2 = new Turtle(AnimalType.REPTILE, 0);

		Ticket ticket1 = new Ticket(10, Date.valueOf("2017-03-30"), TicketType.NORMAL);
		Visitor visitor1 = new Visitor("Pawel", ticket1, true);

		Ticket ticket2 = new Ticket(10, Date.valueOf("2017-03-31"), TicketType.NORMAL);
		Visitor visitor2 = new Visitor("Grzegorz", ticket2, true);

		Ticket ticket3 = new Ticket(10, Date.valueOf("2017-03-29"), TicketType.NORMAL);
		Visitor visitor3 = new Visitor("Michal", ticket3, true);

		Staff staff1 = new Staff("Ochroniarz Tomek");
		Staff staff2 = new Staff("Ochroniarz Rysiu");

		// set na false ze wyszedl z zoo ale kupil bilet
		// visitor2.setPresent(false);
		// visitor3.setPresent(false);

		zoo.addAnimals(turtle1);
		zoo.addAnimals(turtle2);
		zoo.printAnimals();

		zoo.addStaff(staff1);
		zoo.addStaff(staff2);
		zoo.printStaff();

		zoo.addVisitors(visitor1);
		zoo.addVisitors(visitor2);
		zoo.addVisitors(visitor3);

		zoo.printVisitors();

		zoo.printNumberOfTicketsBought();

	}
}