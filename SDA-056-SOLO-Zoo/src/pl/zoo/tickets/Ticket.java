package pl.zoo.tickets;

import java.sql.Date;

public class Ticket {

	private double price = 10.0;
	private Date bought;
	private TicketType type;
	private static int countTicketsBought;

	public Ticket(double price, Date bought, TicketType type) {
		super();
		this.price = price;
		this.bought = bought;
		this.type = type;
		countTicketsBought++;
	}

	public double getPrice() {
		return price;
	}

	public Date getBought() {
		return bought;
	}

	public TicketType getType() {
		return type;
	}

	public static int getCountTicketsBought() {
		return countTicketsBought;
	}

	
	
}
