package pl.zoo.animals;

// zrobil dziedziczenie zeby zablokowac PersonInterface od aplikowania Viewable
public interface Viewable extends AnimalInterface {

	public int getViews();
	
}
