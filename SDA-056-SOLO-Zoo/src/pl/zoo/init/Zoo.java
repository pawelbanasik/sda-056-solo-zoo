package pl.zoo.init;

import java.util.LinkedList;
import java.util.List;

import pl.zoo.animals.AnimalInterface;
import pl.zoo.people.Staff;
import pl.zoo.people.Visitor;
import pl.zoo.tickets.Ticket;

public class Zoo {

	private List<Staff> staff = new LinkedList<>();
	private List<Visitor> visitors = new LinkedList<>();
	private List<AnimalInterface> animals = new LinkedList<>();
	private int numOfPeople = 0;

	public Zoo(int numOfPeople) {
		this.numOfPeople = numOfPeople;

	}

	public void init() {
	}

	public void addVisitors(Visitor visitor) {
		if (this.numOfPeople > 0) {
			visitors.add(visitor);
			this.numOfPeople--;
		
		} else {
			System.out.println("No tickets available.");
		}

	}

	public void addAnimals(AnimalInterface animalInterface) {
		animals.add(animalInterface);
	}

	public void addStaff(Staff staffMember) {
		staff.add(staffMember);
	}

	public void visit(Visitor visitor, AnimalInterface animal) {

	}

	public int getNumOfPeople() {
		return numOfPeople;
	}

	public void printVisitors() {
		System.out.println();
		System.out.println("Only visitors: ");

		for (int i = 0; i < visitors.size(); i++) {
			if (visitors.get(i).isPresent()) {
				System.out
						.println("Visitor number[" + i + "] " + visitors.get(i).getClass().getSimpleName() + ", Name: "
								+ visitors.get(i).getName() + ", Ticket type: " + visitors.get(i).getTicket().getType()
								+ ", Ticket price: " + visitors.get(i).getTicket().getPrice()
								+ ", Ticket date of sale: " + visitors.get(i).getTicket().getBought()
								+ ", Visitor present at zoo: " + visitors.get(i).isPresent());
			}
		}
	}

	public void printStaff() {
		System.out.println();
		System.out.println("Only staff: ");
		for (int i = 0; i < staff.size(); i++) {
			System.out.println("Staff number[" + i + "] " + staff.get(i).getClass().getSimpleName() + ", Name: "
					+ staff.get(i).getName());

		}
	}

	public void printNumberOfTicketsBought() {
		System.out.println();
		System.out.println("Total number of tickets bought: " + Ticket.getCountTicketsBought());
	}

	public void printAnimals() {
		System.out.println();
		System.out.println("Animals in zoo: ");
		for (int i = 0; i < animals.size(); i++) {
			System.out.println("Animal number[" + i + "] Animal: " + animals.get(i).getClass().getSimpleName()
					+ ", Type: " + animals.get(i).getType());
		}
	}

}
