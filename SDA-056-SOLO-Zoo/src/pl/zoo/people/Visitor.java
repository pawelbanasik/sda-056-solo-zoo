package pl.zoo.people;

import pl.zoo.tickets.Ticket;

public class Visitor implements PersonInterface {

	private String name;
	private Ticket ticket;
	private boolean present;

	public Visitor() {

	}

	public Visitor(String name, Ticket ticket, boolean present) {
		super();
		this.name = name;
		this.ticket = ticket;
		this.present = present;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public boolean isPresent() {
		return present;
	}

	public void setPresent(boolean present) {
		this.present = present;
	}
	
	

}
